;= @echo off
;= rem Call DOSKEY and use this file as the macrofile
;= %SystemRoot%\system32\doskey /listsize=1000 /macrofile=%0%
;= rem In batch mode, jump to the end of the file
;= goto:eof
;= Add aliases below here
e.=explorer .
gl=git log --oneline --all --graph --decorate  $*
ls=ls --show-control-chars -F --color $*
ll=ls -al --show-control-chars -F --color $*
pwd=cd
clear=cls
unalias=alias /d $1
vi=vim $*
cmderr=cd /d "%CMDER_ROOT%"

REM alias perso
windowsterminalexample=wt --title "title" --tabColor #62026b wsl -d arch ; sp -D -H ; sp -D -H
gitfixauthor=git -c rebase.instructionFormat="%s%nexec GIT_COMMITTER_DATE='%cD' GIT_AUTHOR_DATE='%aD' git commit --amend --no-edit --reset-author" rebase -f --root
