# dotfiles

All my configuration files.

## Main software

- wm : `sway`
- status bar : `waybar`
- term : `alacritty`
- shell : `zsh` + `omz`
- launcher : `rofi`

## Styles

![styles](https://i.imgur.com/KUGUcbe.png)

## Packages

- `alacritty` as terminal emulator (wayland compatible)
- `arch-wiki-docs`, `arch-wiki-lite`, `man-db`, `man-pages`, `tldr` as documentation providers
- `ark` as archive manager
- `blueman` as bluetooth manager
- `cronie` as CRON replacement
- `dolphin`, `vifm` as file explorers
- `exa` as ls replacement
- `feh`, `gwenview` as image viewers
- `filelight`, `ncdu` as disk space viewers
- `firefox`, `chromium`, `lynx` as browsers
- `flat-remix-gtk` as GTK icons
- `gimp` as image editor
- `gparted` as partition manager
- `grim` + `slurp` as screenshot manager
- `htop`, `ksysguard`, `gotop` as system monitor
- `lxappearance` as GTK style manager
- `lxsession` as polkit
- `ly` as display manager (login)
- `mako` as notifications viewer
- `neofetch` as system summary
- `nerd-fonts-complete` as powerline fonts provider
- `netctl` as network manager
- `obs-studio` as screen recorder
- `pacman`, `paru` as package manager
- `papirus-icon-theme` as icon theme
- `pulseaudio` as audio manager
- `qt5ct` as QT5 style manager
- `rofi` as app launcher
- `signal-desktop`, `teams`, `telegram-desktop` as social chats
- `sway` as desktop manager (wayland)
- `swaylock-effects-git` as screen locker
- `thunderbird` as mail client
- `tig` as visual git client
- `transmission-cli`, `transmission-qt` as torrent downloader
- `vim`, `sublime-text-4`, `visual-studio-code-bin` as text editors
- `virt-manager`, `vagrant`, `virtualbox` as virtual machines managers
- `vlc`, `mpv` as video players
- `waybar` as sway status bar
- `zathura` as pdf viewer
- `zsh`, `fish` as shells
