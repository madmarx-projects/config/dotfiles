#!/bin/sh

# Times the screen off and puts it to background
swayidle \
    timeout 300 'swaymsg "output * dpms off"' \
    resume 'swaymsg "output * dpms on"' &

# Locks the screen immediately
swaylock --clock --indicator --screenshots --effect-scale 0.4 --effect-vignette 0.6:0.5 --effect-blur 4x2 --datestr "%a %d/%m/%Y" --timestr "%k:%M"

# Kills last background task so idle timer doesn't keep running
kill %%

