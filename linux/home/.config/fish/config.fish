# misc
set fish_greeting ""

# aliases
alias a 'paru '
alias ls 'exa'
alias ll 'exa --long --group --icons --git --no-time'
alias ll1 'exa --long --group --tree --level=1 --icons --git --no-time'
alias ll2 'exa --long --group --tree --level=2 --icons --git --no-time'
alias ll3 'exa --long --group --tree --level=3 --icons --git --no-time'
alias ll4 'exa --long --group --tree --level=4 --icons --git --no-time'
alias .. 'cd ..'
#alias cat 'highlight'
alias ip 'ip --color=auto'
alias grep 'grep --color=auto'
alias diff 'diff --color=auto'

