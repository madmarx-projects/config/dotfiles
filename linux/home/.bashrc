# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# If id command returns zero, you got root access.
if [ $(id -u) -eq 0 ];
then # you are root, set red colour prompt
  PS1="\\[$(tput setaf 1)\\]\\u@\\h:\\w #\\[$(tput sgr0)\\]"
else # normal
  # PS1="[\\u@\\h:\\w] $"
  PS1="\[$(tput bold)\]\[\033[38;5;202m\]\u\[$(tput bold)\]\[\033[38;5;15m\]@\[$(tput bold)\]\[\033[38;5;78m\]\H\[$(tput sgr0)\]\[\033[38;5;15m\]:\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;6m\][\w]\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;15m\]\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"
fi

# aliases
alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
