# Tips and tricks when installing a new system

## Archlinux post install

1. Networking
    - `systemctl enable dhcpcd --now`
    - `cp /etc/netctl/examples/xxxx /etc/netctl/mad-xxxx`
    - configure that profile
    - `netctl enable mad-xxxx`
1. Packages
    - `yay`
        - `git clone https://aur.archlinux.org/yay.git`
        - `cd yay`
        - `makepkg -si`
    - `sudo reflector --sort rate --save /etc/pacman.d/mirrorlist`
    - `git` `vim` `chezmoi` `fish`
1. Add user
    - `useradd -m -G wheel -s /usr/bin/fish madmarx`
    - `passwd madmarx`
1. Load config files
    - `chezmoi init https://gitlab.com/madmarx-projects/config/dotfiles-chezmoi.git`
    - `chezmoi apply`
    - edit config to get graphical packages config or not :
        - `chezmoi edit ~/.local/share/chezmoi/chezmoi.toml.tmpl`

### Get colors in `pacman` and `yay`

In `/etc/pacman.conf` uncomment `#Color` :

``` shell
# Misc options
#UserSysLog
Color
# ...
```

## Alpine post install

If `TERM` is not comptible (eg. `cygwin`), edit `/etc/profile` like so

``` shell
export TERM=ansi
```

