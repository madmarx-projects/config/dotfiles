"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Basics {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax on
filetype plugin indent on
set encoding=utf-8

set autoindent
set backspace=indent,eol,start
set hidden          " hidden buffers
set hlsearch        " search highlight
set incsearch
set laststatus=2

" set noshowmode
set noundofile
set number
"set relativenumber
set fixendofline
set wildmenu
set autochdir
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile

set tabstop=2       " The width of a TAB is set to 2.
                    " Still it is a \t. It is just that
                    " Vim will interpret it to be having
                    " a width of 2.
set softtabstop=2   " Sets the number of columns for a TAB.
set shiftwidth=2    " Indents will have a width of 2.
set expandtab       " Expand TABs to spaces.
set omnifunc=syntaxcomplete#Complete
set completeopt=longest,menuone

"set cursorline      " highlight current line

"set termguicolors   " better theme support
set scrolloff=5     " Keep 5 lines below and above the cursor
set mouse=a
set ttymouse=sgr

set ignorecase      " ignore case in search
set smartcase       " dont ignore case if search starts with capital

set splitbelow      " more intuitive splits location
" set splitright      " more intuitive splits location

set sessionoptions=blank,winsize,tabpages,resize

" auto fold and unfold
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Basics }
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Plugins {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')
  " Plug 'arcticicestudio/nord-vim'
  Plug 'preservim/nerdtree'
  Plug 'michaeljsmith/vim-indent-object'
  Plug 'ap/vim-css-color'
  Plug 'tpope/vim-surround'
  " Plug 'tpope/vim-commentary' " casse-couille car commente avec ENTER
  Plug 'tpope/vim-fugitive'
  Plug 'tpope/vim-repeat'
  Plug 'jremmen/vim-ripgrep'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'mbbill/undotree'
  Plug 'ntpeters/vim-better-whitespace'
  Plug 'udalov/kotlin-vim'
call plug#end()
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Plugins }
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Theme {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try
  " colorscheme nord
  colorscheme palenight
catch /^Vim\%((\a\+)\)\=:E185/
  " deal with it
endtry

" let g:nord_cursor_line_number_background = 1
" let g:nord_bold_vertical_split_line = 1
" let g:nord_italic = 1
let g:palenight_terminal_italics = 1

let g:airline_powerline_fonts = 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Theme }
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Shortcuts native {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let mapleader = " "

" buffer
nnoremap gb :ls<CR>:b<Space>

" finder
set path=.,**
nnoremap <leader>f :find 
nnoremap <leader>s :sfind 
nnoremap <leader>v :vert sfind 
nnoremap <leader>t :tabfind 

" deactivate arrow navigation {
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
" }
" deactivate mouse navigation {
:nmap <LeftMouse> <nop>
:imap <LeftMouse> <nop>
:vmap <LeftMouse> <nop>
:nmap <2-LeftMouse> <nop>
:imap <2-LeftMouse> <nop>
:vmap <2-LeftMouse> <nop>
" }

" remap code completion to Ctrl+Space
inoremap <Nul> <C-n>

" Wildmenu {
set wildmode=list:full
set wildignorecase
set wildignore=*.swp,*.bak
set wildignore+=*.pyc,*.class,*.sln,*.Master,*.csproj,*.csproj.user,*.cache,*.dll,*.pdb,*.min.*
set wildignore+=*/.git/**/*,*/.hg/**/*,*/.svn/**/*,*/node_modules/**/*
set wildignore+=tags
set wildignore+=*.tar.*
" }

" remap Enter behavior in popup completion menu : Enter accepts entry {
" inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  " \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
" inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  " \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
" }

" toogle search hightlights
nnoremap <F3> :set hlsearch!<CR>

" tab navigation like Firefox {
nnoremap <C-S-tab> :tabprevious<CR>
nnoremap <C-tab>   :tabnext<CR>
nnoremap <C-t>     :tabnew<CR>
inoremap <C-S-tab> <Esc>:tabprevious<CR>i
inoremap <C-tab>   <Esc>:tabnext<CR>i
inoremap <C-t>     <Esc>:tabnew<CR>
" }

" window navigation {
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>u :UndotreeShow<CR>
" }

" show file explorer on the left
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 40<CR>

nnoremap <silent> <Leader>+ : vertical resize +5<CR>
nnoremap <silent> <Leader>- : vertical resize -5<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Shortcuts native }
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Shortcuts plugins {
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" NERDTree {
nnoremap <C-n> :NERDTreeToggle<CR>

let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let NERDTreeShowLineNumbers=1
let NERDTreeShowHidden=0
let NERDTreeMinimalUI = 0
" }

" remap gcc commenting command from plugin commentary
nmap <C-m> gcc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" =>  Shortcuts plugins }
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

