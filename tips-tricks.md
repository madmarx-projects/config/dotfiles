# Tips and tricks

## Useful shortcuts

### General

- `Shift + scroll` -> Scroll horizontal

### Terminal

- `Ctrl + D` -> Quitter
- `Ctrl + L` -> Clear console
- `Ctrl + A` -> Aller au début
- `Ctrl + E` -> Aller à la fin
- `Ctrl + R` -> Rechercher parmi les dernières commandes exécutées
- `Ctrl + Z` -> Mettre le programme courant en background ($ `fg` pour le restaurer)
- `Ctrl + U` -> Supprimer jusqu'au début
- `Ctrl + K` -> Supprimer jusqu'à la fin
- `Ctrl + Y` -> Coller ce qui a été supprimé
- `Ctrl + P` -> Commande précédente
- `Ctrl + N` -> Commande suivante
- `Ctrl + B` -> Déplacer d'un caractère à gauche
- `Ctrl + F` -> Déplacer d'un caractère à droite
- `Ctrl + ←` -> Déplacer d'un mot à gauche
- `Alt + B` -> Déplacer d'un mot à gauche
- `Ctrl + →` -> Déplacer d'un mot à droite
- `Alt + F` -> Déplacer d'un mot à droite
- `Ctrl + Backspace` -> Supprimer un mot
- `Alt + Shift + .` -> Insère le dernier paramètre de la dernière commande exécutée

### Windows

- `Win + E` -> Explorateur de fichiers
- `Win + I` -> Paramètres Windows
- `Win + L` -> Verrouiller
- `Win + P` -> Changer le mode de présentation des écrans
- `Win + V` -> Clipboard
- `Win + ;` -> Emojis
- `Win + %` -> FancyZones (PowerToys)
- `Win + 1|2|3|4` -> Ouvrir la 1ère|2ème|etc app de la task bar
- `Win + Shift + B` -> Réinitialiser les drivers GPU
- `Win + Shift + C` -> Color-picker (PowerToys)
- `Win + Shift + F` -> Garder au premier plan (PowerToys)
- `Win + Shift + S` -> Screenshot
- `Win + Shift + T` -> OCR (PowerToys)
- `Win + Shift + →` -> Déplacer la fenêtre à la même place sur l'écran suivant
- `Ctrl + Shift + Esc` -> Ouvrir le gestionnaire de tâches
- `Ctrl + Win + →` -> Changer de bureau virtuel

### Explorateur Windows

- `Ctrl + Shift + N` -> Nouveau dossier
- `Ctrl + Shift + T` -> Nouveau fichier texte (qttabbar)

## Software

### Youtube-dl

Télécharger une playlist (ou chaine)

``` bash
yt-dlp --playlist-reverse -o C:\local\folder\%(uploader)s\%(playlist_index)s_%(title)s (%(upload_date)s).%(ext)s" https://www.youtube.com/user/<chaine>/videos
```

